const server = require('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.should();
chai.use(chaiHttp);

describe('Make sure the webserver is running', () => {
  it('This test always returns an error but should not', () => {
    true.should.equal(false);
  });

  it('Status on /ok', () => {
    chai.request(server).get('/ok').end((err, res) => {
      res.should.have.status(200);
      res.body.should.be.a('string');
      res.body.should.equal('The webserver is correctly running');
    })
  });

  it('Status on /error', () => {
    chai.request(server).get('/error').end((err, res) => {
      res.should.have.status(200);
    })
  });
});

describe("Check if user exists", () => {
  it('User in list', () => {
    const userList = [
      { login: 'gowoons' },
      { login: 'randompersonne' }
    ];
    for(user of userList) {
      chai.request(server).post('/user').set('content-type', 'application/json').send(user).end((err, res) => {
        res.should.have.status(200);
      });
    };
  });
});

describe("Check if admin is valid", () => {
  it('User is admin', () => {
    const credentials = {
      login: 'lej',
      password: 'lacicdcestbien'
    };
    chai.request(server).post('/admin').set('content-type', 'application/json').send(credentials).end((err, res) => {
      res.should.have.status(200);
    });
  });
});
